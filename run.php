<?php

namespace Sprinkle;

require __DIR__ . '/vendor/autoload.php';

$elevatorController = new ElevatorController();

$user1 = new User(1, 4);
$user2 = new User(3, 2);
$user3 = new User(4, 1);

$elevatorController->addUser($user1);
$elevatorController->addUser($user2);
$elevatorController->addUser($user3);

while(true) {
    $elevatorController->run();
}