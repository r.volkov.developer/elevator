<?php
namespace Sprinkle;

/**
 * Class User
 * @package Sprinkle
 */
class User
{
    /**
     * @var int
     */
    private $callLevel;
    /**
     * @var int
     */
    private $targetLevel;

    /**
     * User constructor.
     * @param int $callLevel
     * @param int $targetLevel
     */
    public function __construct(int $callLevel, int $targetLevel)
    {
        if (!in_array(
                $targetLevel,
                range(ElevatorController::LOW_LEVEL_VALUE, ElevatorController::HIGH_LEVEL_VALUE)
            )
        ) {
            throw new \DomainException('Unacceptable level value');
        }

        if ($callLevel === $targetLevel) {
            throw new \DomainException('User is not adequate');
        }

        $this->callLevel = $callLevel;
        $this->targetLevel = $targetLevel;
    }

    /**
     * @return mixed
     */
    public function getCallFloor(): int
    {
        return $this->callLevel;
    }

    /**
     * @return mixed
     */
    public function getTargetFloor(): int
    {
        return $this->targetLevel;
    }
}