<?php

namespace Sprinkle;

/**
 * Class ElevatorController
 * @package Sprinkle
 */
class ElevatorController
{
    const LOW_LEVEL_VALUE = 1;
    const HIGH_LEVEL_VALUE = 4;

    /**
     * @var int
     */
    private $nextFloor = 1;
    /**
     * @var int
     */
    private $currentFloor = 1;
    /**
     * @var int
     */
    private $finalFloor = 1;

    /**
     * @return int
     */
    public function getCurrentFloor(): int
    {
        return $this->currentFloor;
    }

    /**
     * @return int
     */
    public function getNextFloor(): int
    {
        return $this->nextFloor;
    }

    /**
     * @var Elevator
     */
    private $elevator;
    /**
     * @var User[]
     */
    private $users = [];

    /**
     * ElevatorController constructor.
     */
    public function __construct()
    {
        $this->elevator = new Elevator();
    }

    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * @return array
     */
    private function getUsersToLoad(): array
    {
        $usersToLoad = array_filter($this->users, function($user) {
            return $user->getCallFloor() === $this->currentFloor;
        });

        $this->users = array_diff_key($this->users, $usersToLoad);

        return $usersToLoad;
    }

    /**
     * @return int
     */
    private function calculateNextFloor(): int
    {
        if ($this->finalFloor === $this->currentFloor) {
            $this->finalFloor = $this->getFinalFloor();
        }

        return $this->nextFloor = $this->getNextFloorByUsers();
    }

    /**
     * @return int
     */
    private function getNextFloorByUsers(): int
    {
        $nextFloor = $this->finalFloor;

        //Ищет ближайший этаж с которого сделали вызов между текущим и финальным
        foreach ($this->users as $user) {
            if ($this->finalFloor > $this->currentFloor
                && $user->getCallFloor() > $this->currentFloor
                && $user->getCallFloor() < $this->finalFloor
                && $user->getCallFloor() < $nextFloor
            ) {
                $nextFloor = $user->getCallFloor();
            }

            if ($this->finalFloor < $this->currentFloor
                && $user->getCallFloor() < $this->currentFloor
                && $user->getCallFloor() > $this->finalFloor
                && $user->getCallFloor() > $nextFloor
            ) {
                $nextFloor = $user->getCallFloor();
            }
        }

        //Ищет ближайший этаж на котором выходят между текущим и финальным
        foreach ($this->elevator->getUsers() as $user) {
            if ($this->finalFloor > $this->currentFloor
                && $user->getTargetFloor() > $this->currentFloor
                && $user->getTargetFloor() < $this->finalFloor
                && $user->getTargetFloor() < $nextFloor
            ) {
                $nextFloor = $user->getTargetFloor();
            }

            if ($this->finalFloor < $this->currentFloor
                && $user->getTargetFloor() < $this->currentFloor
                && $user->getTargetFloor() > $this->finalFloor
                && $user->getTargetFloor() > $nextFloor
            ) {
                $nextFloor = $user->getTargetFloor();
            }
        }

        return $nextFloor;
    }

    /**
     * @return int
     */
    private function getFinalFloor(): int
    {
        if (!empty($this->elevator->getUsers()) && !empty($this->users)) {
            return max(
                current($this->elevator->getUsers())->getTargetFloor(),
                current($this->users)->getCallFloor()
            );
        }

        if (!empty($this->elevator->getUsers())) {
            return current($this->elevator->getUsers())->getTargetFloor();
        } else {
            return current($this->users)->getCallFloor();
        }
    }

    private function triggerFloorArriving()
    {
        $this->getNewUsers();

        $leavingUsersCount = $this->elevator->releaseUsers($this->currentFloor);
        if ($leavingUsersCount) {
            echo "Лифт покинуло {$leavingUsersCount} человек(а)" . PHP_EOL;
        }

        $usersToLoad = $this->getUsersToLoad();

        $this->elevator->loadUsers($usersToLoad);
        if ($usersToLoad) {
            echo 'В лифт заходит ' . count($usersToLoad) . ' человек(а)' . PHP_EOL;
        }
    }

    private function getNewUsers()
    {
        echo 'Добавьте пользователя, вызвавшего лифт, введя этаж вызова и этаж назначения через пробел'
            . PHP_EOL
            . 'Для завершения ввода нажмите Enter'
            . PHP_EOL;
        while($line = trim(fgets(STDIN))) {
            $this->users[] = new User(...explode(' ', $line));
        }
    }

    public function run()
    {
        $this->currentFloor = $this->nextFloor;

        echo 'Лифт находится на этаже ' . $this->currentFloor . PHP_EOL . PHP_EOL;
        $this->triggerFloorArriving();

        while(empty($this->users) && empty($this->elevator->getUsers())) {
            echo 'Лифт пуст.' . PHP_EOL;
            $this->getNewUsers();
        }

        $this->nextFloor = $this->calculateNextFloor();
        echo 'Следующий этаж - ' . $this->nextFloor . PHP_EOL;

        for($i = abs(($this->nextFloor - $this->currentFloor) * 4); $i > 0; $i--) {
            echo "Прибытие на следующий этаж через {$i} секунд" . PHP_EOL;
            sleep(1);
        }
    }
}
