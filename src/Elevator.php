<?php
namespace Sprinkle;

/**
 * Class Elevator
 * @package Sprinkle
 */
class Elevator
{
    /**
     * @var array
     */
    private $users = [];

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param array $incomingUsers
     */
    public function loadUsers(array $incomingUsers)
    {
        $this->users = array_merge($this->users, $incomingUsers);
    }

    /**
     * @param int $currentFloor
     * @return int
     */
    public function releaseUsers(int $currentFloor): int
    {
        $leavingUsers = 0;
        foreach ($this->users as $key => $user) {
            if ($currentFloor === $user->getTargetFloor()) {
                unset($this->users[$key]);
                $leavingUsers++;
            }
        }

        return $leavingUsers;
    }
}
